package cn.ibspayingcloud.verificationcode.Util;

import cn.ibspayingcloud.verificationcode.entity.VerificationCode;
import cn.ibspayingcloud.verificationcode.repositories.VerificationCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * author @ WQ Shang
 */
public class VerificationCodeRedisUtil {


    @Autowired
    private static VerificationCodeRepository verificationCodeRepository;

    public static VerificationCode setVerficationCode(VerificationCode verificationCode) {
        String code;
        String key = verificationCode.getVerifyAddress() + verificationCode.getCodePurposeEnum();
        VerificationCode result = verificationCodeRepository.findByVerifyAddressAndCodePurpose(
                verificationCode.getVerifyAddress(), verificationCode.getCodePurposeEnum());
        if (result == null) {
            code = VerificationCodeProvideUtil.getVerificationCode();
            verificationCode.setVerificationCode(code);
            verificationCodeRepository.save(verificationCode);
        } else {
            verificationCode = result;
        }
        return verificationCode;
    }

    public static boolean checkVerificationCode(VerificationCode verificationCode) {
        VerificationCode result = verificationCodeRepository.findByVerifyAddressAndCodePurpose(
                verificationCode.getVerifyAddress(), verificationCode.getCodePurposeEnum());
        if (result != null) {
            return true;
        } else {
            return false;
        }
    }
}
