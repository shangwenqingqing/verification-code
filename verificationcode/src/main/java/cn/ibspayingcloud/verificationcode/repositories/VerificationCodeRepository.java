package cn.ibspayingcloud.verificationcode.repositories;

import cn.ibspayingcloud.verificationcode.entity.CodePurposeEnum;
import cn.ibspayingcloud.verificationcode.entity.VerificationCode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VerificationCodeRepository extends CrudRepository<VerificationCode, String> {
    VerificationCode findByVerifyAddressAndCodePurpose(String verifyAddress, CodePurposeEnum codePurposeEnum);
}
