package cn.ibspayingcloud.verificationcode.entity;

public enum CodeStyle {
    PURE_NUM,
    PURE_LETTER,
    MIX
}
