package cn.ibspayingcloud.verificationcode.Util;

import java.util.Random;

/**
 * author @ WQ Shang
 */

public class VerificationCodeProvideUtil {
    private static String base;
    private static int length;

    public VerificationCodeProvideUtil(String base, int length) {
        this.base = base;
        this.length = length;
    }

    public static String getVerificationCode() {//length表示生成字符串的长度
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
}
