package cn.ibspayingcloud.verificationcode.entity;

/**
 * author @ WQ Shang
 */

import cn.ibspayingcloud.verificationcode.Util.VerificationCodeProvideUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * author @ WQ Shang
 */
@Configuration

@ConditionalOnMissingBean(VerificationCodeProvideUtil.class)
@EnableConfigurationProperties(VerificationCodeStyleProperties.class)
public class VerificationCodeUtilConfiguration {

    private final VerificationCodeStyleProperties verificationCodeStyleProperties;

    public VerificationCodeUtilConfiguration(
            VerificationCodeStyleProperties verificationCodeStyleProperties) {
        this.verificationCodeStyleProperties = verificationCodeStyleProperties;
    }

    @Bean
    public VerificationCodeProvideUtil printAfterInitBean() {
        String base = null;
        CodeStyle codeStyle = verificationCodeStyleProperties.getCodeStyle();
        switch (codeStyle) {
            case MIX:
                base = "abcdefghijklmnopqrstuvwxyz0123456789";
                break;
            case PURE_NUM:
                base = "0123456789";
                break;
            case PURE_LETTER:
                base = "abcdefghijklmnopqrstuvwxyz";
                break;
            default:
                break;
        }
        int length = verificationCodeStyleProperties.getLength();
        return new VerificationCodeProvideUtil(base,length);
    }

}
