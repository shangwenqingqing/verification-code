package cn.ibspayingcloud.verificationcode.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

/**
 * author @ WQ Shang
 */
@RedisHash("verificationCodes")
public class VerificationCode {
    @Id
    String id;
    private CodePurposeEnum codePurposeEnum;
    private String verifyAddress;
    private java.lang.String verificationCode;

    public VerificationCode() {
    }

    public VerificationCode(CodePurposeEnum codePurposeEnum, String verifyAddress) {
        this.codePurposeEnum = codePurposeEnum;
        this.verifyAddress = verifyAddress;
    }

    public CodePurposeEnum getCodePurposeEnum() {
        return codePurposeEnum;
    }

    public void setCodePurposeEnum(CodePurposeEnum codePurposeEnum) {
        this.codePurposeEnum = codePurposeEnum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVerifyAddress() {
        return verifyAddress;
    }

    public void setVerifyAddress(String verifyAddress) {
        this.verifyAddress = verifyAddress;
    }

    public java.lang.String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(java.lang.String verificationCode) {
        this.verificationCode = verificationCode;
    }
}
