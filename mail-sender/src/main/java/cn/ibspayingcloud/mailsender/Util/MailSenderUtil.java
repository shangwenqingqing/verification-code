package cn.ibspayingcloud.mailsender.Util;


import cn.ibspayingcloud.mailsender.entity.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

/**
 * author @ WQ Shang
 */
public class MailSenderUtil {

    @Autowired
    private static JavaMailSender mailSender;

    /**
     * 修改application.properties的用户，才能发送。
     */
    public static Email sendSimpleEmail(Email email) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(email.getFrom());//发送者
        message.setSubject(email.getSubject());//邮件主题
        message.setTo(email.getTo());//接收者
        message.setText(email.getText());//邮件内容
        System.out.println("email = " + email);
        mailSender.send(message);//发送邮件
        return email;
    }
}
