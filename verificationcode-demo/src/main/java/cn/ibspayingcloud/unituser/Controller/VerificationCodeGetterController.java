package cn.ibspayingcloud.unituser.Controller;

import cn.ibspayingcloud.mailsender.Util.MailSenderUtil;
import cn.ibspayingcloud.mailsender.entity.Email;
import cn.ibspayingcloud.verificationcode.Util.VerificationCodeRedisUtil;
import cn.ibspayingcloud.verificationcode.entity.CodePurposeEnum;
import cn.ibspayingcloud.verificationcode.entity.VerificationCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * author @ WQ Shang
 */

@RestController
public class VerificationCodeGetterController {    VerificationCodeRedisUtil verificationCodeRedisUtil;


    @GetMapping("/getVerificationCode")
    public String test(@RequestParam String to, @RequestParam String codePurpose) {
        Email email = new Email();
        VerificationCode verificationCode = new VerificationCode(CodePurposeEnum.valueOf(codePurpose), to);
        verificationCode = VerificationCodeRedisUtil.setVerficationCode(verificationCode);
        String code = verificationCode.getVerificationCode();
        email.setSubject("验证码");
        email.setText(code);
        email.setTo(to);
        MailSenderUtil.sendSimpleEmail(email);
        return "发送成功";
    }
}
