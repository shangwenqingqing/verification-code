package cn.ibspayingcloud.verificationcode.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * author @ WQ Shang
 */
@ConfigurationProperties(prefix = "verification-code")
public class VerificationCodeStyleProperties {
    private CodeStyle codeStyle = CodeStyle.MIX;
    private int length = 6;

    public CodeStyle getCodeStyle() {
        return codeStyle;
    }

    public void setCodeStyle(CodeStyle codeStyle) {
        this.codeStyle = codeStyle;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
