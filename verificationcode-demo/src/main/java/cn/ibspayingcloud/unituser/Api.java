package cn.ibspayingcloud.unituser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author YQ.Huang
 */
@SpringBootApplication
public class Api {
    public static void main(String[] args) {
        SpringApplication.run(Api.class, args);
    }
}
